# README #


### What is this repository for? ###

* gappleNerf
* Minecraft plugin for spigot 1.9.2 allowing control/customisation of golden+god apple effects,cooldowns,heal via *commands or yml configuration
* Version 1.0
* author: Tom White
* June 2016

### How do I get set up? ###

* Copy the plugin jar file into your spigot plugins folder
* That's it! Start the server.
* Default values will be loaded, creating your config.yml file.
* Reload the plugin once you have the default config set up
* Change settings via this file, or via in game commands; type /ganerf for a list