/*
   This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
 */

/*gappleNerf 1.0
 * Simple bukkit minecraft plugin to control+customise golden+god apple effects
 * written by Tom White, contact @ T_o_m_white@hotmail.com
 * for minecraft version 1.9.2
 * june 2016
 */

package tom.gappleNerf;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.ArrayList;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.Material;
import org.bukkit.event.Listener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import java.util.HashSet;
import java.util.List;




/*Minecraft plugin to nerf god apples
* created for spiggot 1.9.2
* by tom white
*/

public class gappleMain extends JavaPlugin implements Listener {
	
	//variables for god apples:
	//holds current gapple potion effects
	private ArrayList<String[]> godEffects = new ArrayList<String[]>();
	private int godCooldown=0;//holds the current cooldown of god apples
	private int godHeal=0;//holds the current healing of god apples
	
	//variables for golden apples:
	//holds current gapple potion effects
	private ArrayList<String[]> goldEffects = new ArrayList<String[]>();
	private int goldCooldown=0;//holds the current cooldown of god apples
	private int goldHeal=0;//holds the current healing of god apples
	
	//player cooldowns
	public final HashSet<String> cooldownSetGold = new HashSet<String>();
	public final HashSet<String> cooldownSetGod = new HashSet<String>();
	
	//potion effects in the game, populated on plugin enable
 	private HashSet<String> allowedEffects= new HashSet<String>();//the allowed effects
	
	//When the plugin is enabled
	@Override
	public void onEnable() {
		
		loadSettings();//load the config.yml file settings
		
		//minecraft potion effects (bukkit names)
		allowedEffects.add("ABSORPTION");
		allowedEffects.add("BLINDNESS");
		allowedEffects.add("CONFUSION");
		allowedEffects.add("DAMAGE_RESISTANCE");
		allowedEffects.add("FAST_DIGGING");
		allowedEffects.add("FIRE_RESISTANCE");
		allowedEffects.add("GLOWING");
		allowedEffects.add("HARM");
		allowedEffects.add("HEAL");
		allowedEffects.add("HEALTH_BOOST");
		allowedEffects.add("HUNGER");
		allowedEffects.add("INCREASE_DAMAGE");
		allowedEffects.add("INVISIBILITY");
		allowedEffects.add("JUMP");
		allowedEffects.add("LEVITATION");
		allowedEffects.add("LUCK");
		allowedEffects.add("NIGHT_VISION");
		allowedEffects.add("POISON");
		allowedEffects.add("REGENERATION");
		allowedEffects.add("SATURATION");
		allowedEffects.add("SLOW");
		allowedEffects.add("SLOW_DIGGING");
		allowedEffects.add("SPEED");
		allowedEffects.add("UNLUCK");
		allowedEffects.add("WATER_BREATHING");
		allowedEffects.add("WEAKNESS");
		allowedEffects.add("WITHER");
		
		//registering events
		this.getServer().getPluginManager().registerEvents(this, this);
	}
    
	//utility method checking if a string can be an int for parameter
	//type checking of player commands
	public boolean isInteger(String testString){
		try{
		int a=Integer.parseInt(testString);
			return true;
		}
		catch (Exception e){
			return false;
		}
	}
	
	//loading settings and sets defaults if necessary
	public void loadSettings(){
		
		//default golden + god apple effects
		String[] defaultGoldEffects={"ABSORPTION 0 2400","REGENERATION 1 100"};
		String[] defaultGodEffects={"ABSORPTION 3 2400","REGENERATION  1 400","FIRE_RESISTANCE 0 6000","DAMAGE_RESISTANCE 0 6000"};
		
		//setting defaults if needed
		getConfig().options().copyDefaults(true);
		getConfig().addDefault("god.cooldown",0);
		getConfig().addDefault("god.heal",0);
		getConfig().addDefault("gold.cooldown",0);
		getConfig().addDefault("gold.heal",0);
		getConfig().addDefault("god.effects",defaultGodEffects);
		getConfig().addDefault("gold.effects",defaultGoldEffects);
		
		//loading any configurations to variables
		godCooldown=getConfig().getInt("god.cooldown");
		goldCooldown=getConfig().getInt("gold.cooldown");
		godHeal=getConfig().getInt("god.heal");
		goldHeal=getConfig().getInt("gold.heal");
		List<String> newGoldEffects=getConfig().getStringList("gold.effects");
		List<String> newGodEffects=getConfig().getStringList("god.effects");
		for(String effectString:newGoldEffects){
			String[] effect=effectString.split("\\s+");
			goldEffects.add(effect);
		}
		for(String effectString:newGodEffects){
			String[] effect=effectString.split("\\s+");
			godEffects.add(effect);
		}
		saveConfig();
	}
	
	//when a golden or god apple is eaten
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEat(PlayerItemConsumeEvent eatEvent){
	if(eatEvent.getItem().getType() == Material.GOLDEN_APPLE){
		eatEvent.setCancelled(true);
		Player p = eatEvent.getPlayer();
		p.updateInventory();
		int appleNumber=eatEvent.getItem().getAmount();
		ItemStack newApples = new ItemStack(Material.GOLDEN_APPLE, appleNumber-1);
		
		//applying effects, healing and cooldown:
		if(eatEvent.getItem().getDurability()==1){//god apple
			if((!cooldownSetGod.contains(p.getName()))||(godCooldown==0)) {
				for(String[] effectArray:godEffects){
					PotionEffect effect = new PotionEffect(PotionEffectType.getByName(effectArray[0]), Integer.parseInt(effectArray[2]), Integer.parseInt(effectArray[1]));
					p.addPotionEffect(effect);//give the relevant potion effects
				}
				//applying any healing specified
				if(godHeal!=0){
					if(p.getHealth()+godHeal<=20){
						p.setHealth(p.getHealth()+godHeal);
					}
					else{
						p.setHealth(20);
					}
				}
				//applying (for now) the default hunger boost
				if(p.getFoodLevel()<=16){
					p.setFoodLevel(p.getFoodLevel()+4);
				}
				else{
					p.setFoodLevel(20);
				}
				
				newApples.setDurability((short)1);
				if(godCooldown>0){//if there is a cooldown
					cooldownSetGod.add(p.getName());
				}
				p.getInventory().setItemInHand(newApples);
				final String playerName = p.getName();
				//cooldown mechanic:
				getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
				public void run(){
					cooldownSetGod.remove(playerName);
				}	
				}, (long)godCooldown*20);
			}
			else{//on cooldown
				p.sendMessage("Your enchanted golden apple is on cooldown!");
			}
		}
		else{//gold apple
			if(!cooldownSetGold.contains(p.getName())||(goldCooldown==0)) {
				for(String[] effectArray:goldEffects){
					PotionEffect effect = new PotionEffect(PotionEffectType.getByName(effectArray[0]), Integer.parseInt(effectArray[2]), Integer.parseInt(effectArray[1]));
					p.addPotionEffect(effect);//give the relevant potion effects
				}
				//applying any healing specified
				if(goldHeal!=0){
					if(p.getHealth()+goldHeal<=20){
						p.setHealth(p.getHealth()+godHeal);
					}
					else{
						p.setHealth(20);
					}
				}
				//applying (for now) the default hunger boost
				if(p.getFoodLevel()<=16){
					p.setFoodLevel(p.getFoodLevel()+4);
				}
				else{
					p.setFoodLevel(20);
				}
				newApples.setDurability((short)0);
				if(goldCooldown>0){//if there is a cooldown
					cooldownSetGold.add(p.getName());
				}
				p.getInventory().setItemInHand(newApples);
				final String playerName = p.getName();
				//cooldown mechanic:
				getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable(){
				public void run(){
					cooldownSetGold.remove(playerName);
				}	
				}, (long)goldCooldown*20);
			}
			else{//on cooldown
				p.sendMessage("Your golden apple is on cooldown!");
			}
		}
		p.updateInventory();

	}
	
	else{
		
	}
	}


	
	
	//When the plugin is disabled
	@Override
	public void onDisable() {
	  saveConfig();
	}
	
	//When the plugin receives a /command
    @Override
    public boolean onCommand(CommandSender sender,
            Command command,
            String label,
            String[] args) {
    	
    	//COMMAND TO SET APPLE COOLDOWN
        if (command.getName().equalsIgnoreCase("gasetCooldown")) {
        	if((args.length>1)&&(isInteger(args[1]))){
	        	if(args[0].equalsIgnoreCase("god")){
	        		sender.sendMessage("Setting god apple CD to: "+(Integer.parseInt(args[1]))+" seconds");
	        		this.godCooldown=(Integer.parseInt(args[1]));
	        		getConfig().set("god.cooldown", Integer.parseInt(args[1]));
	        		saveConfig();
	        		}
	        	 else if((args[0].equalsIgnoreCase("gold"))||(args[0].equalsIgnoreCase("golden"))){
		        	sender.sendMessage("Setting gold apple CD to: "+(Integer.parseInt(args[1]))+" seconds");
		        	this.goldCooldown=(Integer.parseInt(args[1]));
	        		getConfig().set("gold.cooldown", Integer.parseInt(args[1]));
	        		saveConfig();
	        	}
	        	else{
	        		sender.sendMessage("Invalid apple type, try gold, golden or god");
	        		sender.sendMessage("e.g. /gasetcooldown god 5");
	        	}
        	}
        	else{
        		sender.sendMessage("Must specify apple type, either gold or god, and cooldown in seconds");
        		sender.sendMessage("e.g. /gasetcooldown god 5");
        	}
        	return true;
        	
        //COMMAND TO REMOVE ALL CURRENTLY SET POTION EFFECTS FROM AN APPLE TYPE
        }  else if (command.getName().equalsIgnoreCase("garemoveEffects")){
        	if(args.length>0){
        		String[] emptyArray={};
	        	if(args[0].equalsIgnoreCase("god")){
	        		int numberRemoved=godEffects.size();
	        		godEffects = new ArrayList<String[]>();
	        		getConfig().set("god.effects", emptyArray);
	        		saveConfig();
	        		sender.sendMessage("Removed " + numberRemoved + " effects from god apples");
	        	} else if((args[0].equalsIgnoreCase("gold"))||(args[0].equalsIgnoreCase("golden"))){
	        		int numberRemoved=goldEffects.size();
	        		goldEffects = new ArrayList<String[]>();
	        		getConfig().set("gold.effects", emptyArray);
	        		saveConfig();
	        		sender.sendMessage("Removed " + numberRemoved + " effects from gold apples");	
	        	}
	        	else{
	        		sender.sendMessage("Invalid apple type, try gold, golden or god");
	        		sender.sendMessage("e.g. /garemoveeffects god");
	        	}
        	}
        	else{
        		sender.sendMessage("Must specify apple type, either gold or god");
        		sender.sendMessage("e.g. /garemoveeffects god");
        	}
        	return true;
        	
        	
        //COMMAND TO ADD A POTION EFFECT TO AN APPLE TYPE
        }  else if (command.getName().equalsIgnoreCase("gaaddEffect")){
        	if((args.length>3)&&(isInteger(args[2]))&&(isInteger(args[3]))){
        		long ticks=(Integer.parseInt(args[3])*20);//command input should be ticks, make it seconds
	        	int level=(Integer.parseInt(args[2]));//the level of the potion effect
	        	if(allowedEffects.contains(args[1].toUpperCase())){
	        		String effect=args[1].toUpperCase();
	        		if(args[0].equalsIgnoreCase("god")){
			        	    String[] tempArray={effect,""+level,""+ticks};
			        	    godEffects.add(tempArray);
			        	    List<String> currentConfig=getConfig().getStringList("god.effects");
			        	    currentConfig.add(effect+" "+level+" "+ticks);
			        	    getConfig().set("god.effects", currentConfig);
			        		saveConfig();
			        		sender.sendMessage("Added effect: " + args[1]+" Level: "+level+" Duration: "+(ticks/20) + "seconds to god apples");
			        	} else if((args[0].equalsIgnoreCase("gold"))||(args[0].equalsIgnoreCase("golden"))){
			        		String[] tempArray={effect,""+level,""+ticks};
			        	    goldEffects.add(tempArray);
			        	    List<String> currentConfig=getConfig().getStringList("god.effects");
			        	    currentConfig.add(effect+" "+level+" "+ticks);
			        	    getConfig().set("gold.effects", currentConfig);
			        		saveConfig();
			        		sender.sendMessage("Added effect: " + args[1]+" Level: "+level+" Duration: "+(ticks/20) + "seconds to gold apples");
			        	}
			        	else{
			        		sender.sendMessage("Invalid apple type, try gold, golden or god");
			        	}
	        	}
	        	else{
	        		sender.sendMessage("Potion effect: "+args[1]+" not valid. Use bukkit format e.g. ABSORPTION or absorption");
	        		sender.sendMessage("e.g. /gaaddeffect god absorption 1 10");
	        	}
        	}
        	else{
        		sender.sendMessage("Must specify apple type, potion effect,level, duration");
        		sender.sendMessage("e.g. /gaaddeffect god absorption 1 10");
        	}
        	return true;
        	
        	
        	
        	
        	
        //SETS A DIRECT HEALING AMOUNT FOR AN APPLE TYPE
        }  else if (command.getName().equalsIgnoreCase("gasetHeal")){
        	if((args.length>1)&&(isInteger(args[1]))){
	        	if(args[0].equalsIgnoreCase("god")){
	        		sender.sendMessage("Setting god apple Heal to: "+(Integer.parseInt(args[1])));
	        		this.godHeal=(Integer.parseInt(args[1]));
	        		if(godHeal>20) godHeal=20;
	        		getConfig().set("god.heal",Integer.parseInt(args[1]));
	        		saveConfig();
	        		}
	        	 else if((args[0].equalsIgnoreCase("gold"))||(args[0].equalsIgnoreCase("golden"))){
		        	sender.sendMessage("Setting gold apple Heal to: "+(Integer.parseInt(args[1])));
		        	this.goldHeal=(Integer.parseInt(args[1]));
		        	if(goldHeal>20) goldHeal=20;
		        	getConfig().set("gold.heal",Integer.parseInt(args[1]));
		        	saveConfig();
	        	}
	        	else{
	        		sender.sendMessage("Invalid apple type, try gold, golden or god");
	        		sender.sendMessage("e.g. /gasetheal god 4");
	        	}
        	}
        	else{
        		sender.sendMessage("Must specify apple type, either gold or god, and Heal as an integer");
        		sender.sendMessage("e.g. /gasetheal god 4");
        	}
        	return true;
        	
        //COMMAND TO DISPLAY CURRENT APPLE EFFECTS
        }  else if (command.getName().equalsIgnoreCase("gashowEffects")){
        	if(args.length>0){
	        	if(args[0].equalsIgnoreCase("god")){
	        		sender.sendMessage("Enchanted golden apple effects currently:");
	        		String messageLine="";
	        		for(String[] nextEffect:godEffects){
	        			messageLine="";
	        			String[] appendedList={"Effect: ","Level: ", "Ticks:"};
	        			int index=0;
	        			for(String word: nextEffect){
	        				messageLine+=appendedList[index]+word+" ";
	        				index++;
	        			}
	        			sender.sendMessage(messageLine);
	        		}
	        		sender.sendMessage("Cooldown: "+((int)godCooldown)+" seconds");
	        		sender.sendMessage("Heal: "+godHeal);
	        	} else if((args[0].equalsIgnoreCase("gold"))||(args[0].equalsIgnoreCase("golden"))){
	        		sender.sendMessage("Golden apple effects currently:");
	        		String messageLine="";
	        		for(String[] nextEffect:goldEffects){
	        			messageLine="";
	        			String[] appendedList={"Effect: ","Level: ", "Ticks:"};
	        			int index=0;
	        			for(String word: nextEffect){
	        				messageLine+=appendedList[index]+word+" ";
	        				index++;
	        			}
	        			sender.sendMessage(messageLine);
	        		}
	        		sender.sendMessage("Cooldown: "+((int)goldCooldown)+" seconds");
	        		sender.sendMessage("Heal: "+goldHeal);
	        	}
	        	else{
	        		sender.sendMessage("Invalid apple type, try gold, golden or god");
	        		sender.sendMessage("e.g. /gashoweffects god");
	        	}
        	}
        	else{
        		sender.sendMessage("Must specify apple type, either gold or god");
        		sender.sendMessage("e.g. /gashoweffects god");
        	}
        	return true;
        } 
        else if (command.getName().equalsIgnoreCase("ganerf")){
        	sender.sendMessage("valid commands: /gashoweffects /garemoveeffects /gaaddeffect /gasetcooldown /gasetheal");
        	return true;
        }
        //no valid command entered
        else{
        	return false;
        }
    }


}
